package com.genspark.CalculatorDemo.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcController {

    @GetMapping("/")
    public String home()
    {
        return "<HTML><H1>Welcome to my calculator</H1></HTML>";
    }

    @GetMapping("/add")
    public int add(int a, int b)
    {
        return a + b;
    }

    @GetMapping("/subtract")
    public int subtract(int a, int b)
    {
        return a - b;
    }

    @GetMapping("/multiply")
    public int multiply(int a, int b)
    {
        return a * b;
    }

    @GetMapping("/divide")
    public int divide(int a, int b)
    {
        if (b == 0)
        {
            throw new RuntimeException("Cannot divide by zero");
        }
        else
        {
            return a / b;
        }
    }

    @GetMapping("/square")
    public int square(int a)
    {
        return a * a;
    }
}
