package com.genspark.InventoryDemo.Service;

import com.genspark.InventoryDemo.Dao.InventoryDao;
import com.genspark.InventoryDemo.Entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.genspark.InventoryDemo.Dao.InventoryDao;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryDao inventoryDao;

    @Override
    public List<Item> getAllItems()
    {
        return this.inventoryDao.findAll();
    }

    @Override
    public Item getItemByID(int givenBarcode)
    {
        Optional<Item> retrievedItem = this.inventoryDao.findById((givenBarcode));
        Item item = null;

        if (retrievedItem.isPresent())
        {
            item = retrievedItem.get();
        }
        else
        {
            throw new RuntimeException("Item not found for: " + givenBarcode);
        }

        return item;
    }

    @Override
    public Item addItem(Item givenItem) {
        return this.inventoryDao.save(givenItem);
    }

    @Override
    public Item updateItem(Item givenItem) {
        return this.inventoryDao.save(givenItem);
    }

    @Override
    public String deleteItem(int givenBarcode)
    {
        this.inventoryDao.deleteById(givenBarcode);

        return "Deleted item successfully";
    }
}
