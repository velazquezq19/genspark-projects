package com.genspark.InventoryDemo.Service;

import com.genspark.InventoryDemo.Entity.Item;

import java.util.List;

public interface InventoryService {
    List<Item> getAllItems();
    Item getItemByID(int givenBarcode);
    Item addItem(Item givenItem);
    Item updateItem(Item givenItem);
    String deleteItem(int givenBarcode);
}
