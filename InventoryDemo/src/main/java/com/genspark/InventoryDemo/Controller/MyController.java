package com.genspark.InventoryDemo.Controller;

import com.genspark.InventoryDemo.Entity.Item;
import com.genspark.InventoryDemo.Service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyController {

    @Autowired
    public InventoryService inventoryService;

    @GetMapping("/")
    public String home()
    {
        return "<HTML><H1>My Inventory Service</H1></HTML>";
    }

    @GetMapping("/inventory")
    public List<Item> getInventory()
    {
        return this.inventoryService.getAllItems();
    }

    @GetMapping("/inventory/{givenBarcode}")
    public Item getItem(@PathVariable String givenBarcode)
    {
        return this.inventoryService.getItemByID(Integer.parseInt(givenBarcode));
    }

    @PostMapping("/inventory")
    public Item addItem(@RequestBody Item givenItem)
    {
        return this.inventoryService.addItem(givenItem);
    }

    @PutMapping("/inventory")
    public Item updateItem(@RequestBody Item givenItem)
    {
        return this.inventoryService.updateItem(givenItem);
    }

    @DeleteMapping("/inventory/{givenBarcode}")
    public String deleteItem(@PathVariable String givenBarcode)
    {
        return this.inventoryService.deleteItem(Integer.parseInt(givenBarcode));
    }
}
