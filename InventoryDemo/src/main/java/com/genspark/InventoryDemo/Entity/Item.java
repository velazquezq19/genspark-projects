package com.genspark.InventoryDemo.Entity;

import javax.persistence.*;

@Entity
@Table(name="tbl_InventoryDemo")
public class Item {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int barcode;
    private String name;
    private int amount;

    public Item() {
    }

    public Item(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
