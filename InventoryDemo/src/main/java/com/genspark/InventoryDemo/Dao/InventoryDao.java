package com.genspark.InventoryDemo.Dao;

import com.genspark.InventoryDemo.Entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryDao extends JpaRepository<Item, Integer> {
}
