import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class InputTest {
    Input input;

    @BeforeEach
    void setUp() {
        input = new Input(1);
    }

    @Test
    void getChosenNumber() {
        assertEquals(1, input.getChosenNumber(), "getChosenNumber test failed");
    }

    @Test
    void setChosenNumber() {
        input.setChosenNumber(2);
        assertEquals(2, input.getChosenNumber(), "setChosenNumber test failed");
    }

    @AfterEach
    void tearDown() {
    }
}