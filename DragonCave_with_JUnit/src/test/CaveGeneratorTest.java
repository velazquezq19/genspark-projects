import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class CaveGeneratorTest {
    CaveGenerator caveGenerator;

    @BeforeEach
    void setUp() {
        caveGenerator = new CaveGenerator();
    }

    @Test
    void getCaveNumber(){
        assertTrue(caveGenerator.getCaveNumber() == 1 || caveGenerator.getCaveNumber() == 2,
                "CaveGenerator test failed.");
    }

    @AfterEach
    void tearDown() {
    }
}