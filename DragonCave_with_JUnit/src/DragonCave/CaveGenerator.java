import java.util.Random;

public class CaveGenerator
{
    Random rand = new Random();
    int caveNumber = rand.nextInt(2) + 1;

    public int getCaveNumber()
    {
        return caveNumber;
    }
}
