public class Input
{
    int chosenNumber;

    public Input(int n)
    {
        this.chosenNumber = n;
    }

    public int getChosenNumber()
    {
        return chosenNumber;
    }

    public void setChosenNumber(int s)
    {
        this.chosenNumber = s;
    }
}

