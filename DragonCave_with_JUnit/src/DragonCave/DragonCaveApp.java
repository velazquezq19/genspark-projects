import java.util.Scanner;
//import java.util.Random;

public class DragonCaveApp {
    public static void main (String[] args) {
        CaveGenerator caveGenerator = new CaveGenerator();

        String intro = "You are in a land full of dragons.  In front of you, \n" +
                "you see two caves.  In one cave the dragon is friendly \n" +
                "and will share his treasure with you.  The other dragon \n" +
                "is greedy and hungry and will eat you on sight.  \n" +
                "Which cave will you go into? (1 or 2)";
        System.out.println(intro);

        boolean flag = true;
        //int userInput = 0;
        while(flag)
        {
            int userInput = 0;
            Input caveChoice = null;
            Scanner getInput = new Scanner(System.in);
            try
            {
                caveChoice = new Input (Integer.parseInt(getInput.nextLine()));
            }
            catch (Exception e)
            {
                System.out.println("Caught Exception: Number Format Exception");
            }

            int choice = caveChoice.getChosenNumber();
            if (choice < 1 || choice > 2)
            {
                System.out.println("Error: number not selected.  Please select 1 or 2.");
            }
            else
            {
                System.out.println("You have chosen: " + caveChoice.getChosenNumber());
                if (choice != caveGenerator.getCaveNumber())
                {
                    String badCave = "You approach the cave...\n" +
                            "It is dark and spooky...\n" +
                            "A large dragon jumps out in front of you!  He opens his jaws and...\n" +
                            "Gobbles you down in one bite!";
                    System.out.println(badCave);
                    flag = false;
                }
                else
                {
                    String goodCave = "It is dark and spooky...\n" +
                            "A large dragon jumps out in front of you!  \n" +
                            "It turns out that they are friendly and wants to share their treasure with you.\n" +
                            "How awesome is that!";
                    System.out.println(goodCave);
                    flag = false;
                }
            }
        }
    }
}
