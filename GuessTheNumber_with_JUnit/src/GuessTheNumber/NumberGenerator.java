import java.util.Random;

public class NumberGenerator
{
    Random rand = new Random();

    int chosenNumber = rand.nextInt(20) + 1;

    public int getChosenNumber()
    {
        return chosenNumber;
    }
}
