public class Player
{
    public String name;

    public Player()
    {
        this.name = "";
    }

    public Player(String playerName)
    {
        this.name = playerName;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String newName)
    {
        this.name = newName;
    }
}

