import java.util.*;

public class GuessApplication {
    public static void main(String[] args) {
        Scanner getInput = new Scanner(System.in);

        System.out.println("Hello!  What is your name?");

        // Gets user input and stores it in Player object.
        Player player = new Player();
        try
        {
            player.setName(getInput.nextLine());
        }
        catch (Exception e)
        {
            System.out.println("Caught Exception: IOException");
        }

        String playGame = "y";
        while (playGame.equalsIgnoreCase("y"))
        {
            NumberGenerator number = new NumberGenerator();

            System.out.println("Well, " + player.getName() + ", I am thinking of a number between 1 and 20.");

            int count = 0;
            boolean flag = false;
            while (count < 6)
            {
                System.out.println("Take a guess.");

                // Sets user input to guess and compares it to the chosen number.
                count++;
                int guess = 0;
                try
                {
                    guess = Integer.parseInt(getInput.nextLine());
                }
                catch (Exception e)
                {
                    System.out.println("Caught Exception: Number Format Exception");
                }

                if (guess == number.getChosenNumber())
                {
                    System.out.println("Good job, " + player.getName() + "!  You guessed my number in " + count + " guesses!");
                    flag = true;
                    break;
                }
                else
                {
                    if (guess < number.getChosenNumber())
                    {
                        System.out.println("Your guess is too low.");
                    }
                    else
                    {
                        System.out.println("Your guess is too high.");
                    }
                }
            }

            // If guess count is greater than 5, say too many guesses
            // and asks to play again or end the game.
            if (count > 5 && !flag)
            {
                System.out.println("Sorry, but you guessed too many times.");
            }
            else
            {
                System.out.println("End of game");
            }

            System.out.println("Would you like to play again? (y or n)");
            try
            {
                playGame = getInput.nextLine();
                if (!playGame.equalsIgnoreCase("y") && !playGame.equalsIgnoreCase("n"))
                {
                    System.out.println("Not one of the choices.  Ending the game.");
                    playGame = "n";
                }
            }
            catch (Exception e)
            {
                System.out.println("Exception: IOException");
            }
        }

    }
}
