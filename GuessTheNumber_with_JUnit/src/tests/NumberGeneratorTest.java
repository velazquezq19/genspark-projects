import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class NumberGeneratorTest {
    NumberGenerator numberGenerator;

    @BeforeEach
    void setUp() {
        numberGenerator = new NumberGenerator();
    }

    @Test
    void getChosenNumber() {
        assertTrue(numberGenerator.getChosenNumber() > 0 && numberGenerator.getChosenNumber() <=20,
                "NumberGenerator test failed");
    }

    @AfterEach
    void tearDown() {
    }
}