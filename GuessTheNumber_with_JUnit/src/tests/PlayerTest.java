import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class PlayerTest {
    Player player;

    @BeforeEach
    void setUp() {
        player = new Player("Quinton");
    }

    @Test
    void getName() {
        assertEquals("Quinton", player.getName(), "Player getName test failed");
    }

    @Test
    void setName() {
        player.setName("Junior");
        assertEquals("Junior", player.getName(), "Player setName test failed");
    }

    @AfterEach
    void tearDown() {
    }
}