import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class WordGeneratorTest {
    WordGenerator testGenerator;

    @BeforeEach
    void setUp() {
        testGenerator = new WordGenerator();
    }

    @Test
    void getWord() {
        assertEquals(testGenerator.getWord(0), "dog", "Word at index does not match.");
        assertEquals(testGenerator.getWord(1), "cat", "Word at index does not match.");
        assertEquals(testGenerator.getWord(2), "fish", "Word at index does not match.");
        assertEquals(testGenerator.getWord(3), "bird", "Word at index does not match.");
        assertEquals(testGenerator.getWord(4), "horse", "Word at index does not match.");
    }

    @Test
    void sizeOfList() {
        assertEquals(testGenerator.sizeOfList(), 5, "Not the correct size");
    }

    @AfterEach
    void tearDown() {
    }
}