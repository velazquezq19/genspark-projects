import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompareLettersTest {
    CompareLetters testCompare;

    @BeforeEach
    void setUp() {
        testCompare = new CompareLetters("cat");
    }

    @Test
    void letterExists() {
        assertEquals(testCompare.letterExists('a'), 1, "Letter doesn't exist or test failed");
    }

    @AfterEach
    void tearDown() {
    }
}