import java.util.*;

public class Main {
    public static void main (String[] args) {
        System.out.println("H A N G M A N");

        WordGenerator words = new WordGenerator();
        Random rand = new Random();

        Scanner getInput = new Scanner(System.in);

        String playGame = "yes";
        
        while(playGame.equalsIgnoreCase("yes"))
        {
            String chosenWord = words.getWord(rand.nextInt(words.sizeOfList()));
            CompareLetters compare = new CompareLetters(chosenWord);
            ArrayList<Character> correctSpots = new ArrayList<>(chosenWord.length());
            for (int i = 0; i < chosenWord.length(); i++)
            {
                correctSpots.add('_');
            }

            String userInput = "";
            ArrayList<Character> incorrectLetters = new ArrayList<>();
            int finished = 0;
            int tries = 0;
            System.out.println("Guess a letter.");
            while (tries < 6 && finished != chosenWord.length())
            {
                try
                {
                    userInput = getInput.nextLine();
                }
                catch (Exception e)
                {
                    System.out.println("Caught Exception: IOException");
                }

                if (userInput.length() > 1)
                {
                    System.out.println("Too many letters!");
                }
                else if (incorrectLetters.contains(userInput.charAt(0)) || correctSpots.contains(userInput.charAt(0)))
                {
                    System.out.println("You have already guesses that letter.  Choose again.");
                }
                else
                {
                    int atIndex = compare.letterExists(userInput.charAt(0));
                    if (atIndex == -1)
                    {
                        tries++;
                        incorrectLetters.add(userInput.charAt(0));
                        System.out.println("Missed letters: " + incorrectLetters);
                        //System.out.println("Wrong letter.  Try again.");
                    }
                    else
                    {
                        correctSpots.set(atIndex, userInput.charAt(0));
                        finished++;
                        System.out.println(correctSpots);
                        //System.out.println("Correct.  Pick another letter.");
                    }
                }
            }

            if (finished == chosenWord.length())
            {
                System.out.println("Yes!  The secret word is \"" + chosenWord + "\"!  You have won!\n" +
                        "Do you want to play again? (yes or no)");
            }
            else
            {
                System.out.println("Sorry, you have reached the maximum number of tries (six).\n" +
                        "Do you want to play again? (yes or no)");
            }
            playGame = getInput.nextLine();
            try
            {
                if(!playGame.equalsIgnoreCase("yes") && !playGame.equalsIgnoreCase("no"))
                {
                    System.out.println("Not one of the choices.  Ending the game.");
                    playGame = "no";
                }
            }
            catch (Exception e)
            {
                System.out.println("Exception: IOException");
            }
        }
    }
}
