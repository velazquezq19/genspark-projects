import java.util.ArrayList;

public class WordGenerator {
    ArrayList<String> wordList;

    public WordGenerator()
    {
        wordList = new ArrayList<String>() {{
            add("dog");
            add("cat");
            add("fish");
            add("bird");
            add("horse");
        }};
    }

    public String getWord(int i)
    {
        return wordList.get(i);
    }

    public int sizeOfList()
    {
        return wordList.size();
    }
}
