import java.util.*;

public class CompareLetters {
    char given;
    char [] word;

    public CompareLetters(String s)
    {
        this.word = s.toCharArray();
    }

    public int letterExists(char c)
    {
        int index = -1;
        for (int i = 0; i < word.length; i++)
        {
            if (c == word[i])
            {
                index = i;
                break;
            }
        }
        return index;
    }
}
