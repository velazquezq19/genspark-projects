package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        Student student = (Student) context.getBean("Student");
        //student.show();
        System.out.println(student.toString());
        //System.out.println(student.getPh());
        //System.out.println( "Hello World!" );
    }
}
